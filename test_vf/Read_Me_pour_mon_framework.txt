		ANDRIANTSIORY Mandresy Tokiniaina   p14A        ETU1396 				

-Vous devez mettre un ".do" a la fin des urls qui appelent des fonctions

-Mettre dans la  librairie de votre projet web les jar suivants:(annot.jar,contr.jar,exc.jar,uti.jar)

-Veuillez mettre ligne par ligne dans config.txt la liste des packages dans les quelles vous mettez vos models

-Ajoutez ceci dans votre web.xml:
	<servlet>
        <servlet-name>FrontServlet</servlet-name>
        <servlet-class>Controller.FrontServlet</servlet-class>
        <init-param>
            <param-name>chemin</param-name>
            <param-value>{le chemin aboslue de votre projet}</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>FrontServlet</servlet-name>
        <url-pattern>*.do</url-pattern>
    </servlet-mapping>

-Anoter les fonctions par:@AnotURL(url="...") pour indiquer l'url qui appelle chaque methodes
-Anoter les fonctions par:@AnotAction(action="/... .jsp")pour definir la page jsp la page jsp dans laquelle le controller va dispacther apres l'appel de fonctions
-Si votre fonction n'est pas void, il doit retourner une classe de type :"ModelView" 
	exemples: 	HashMap<String,Object> data=new HashMap<String,Object>();
        		data.put("donnees",donnees a passée); //donnees= le nom a appelé dans la page jsp pour le getAttribute(""); 
       	 	modeleview.setData(data); // modeleview: variable a retourné
				
		
	