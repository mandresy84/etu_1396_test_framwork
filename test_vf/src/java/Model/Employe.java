/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Annotation.AnotAction;
import Annotation.AnotURL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import utilitaire.ModelView;

/**
 *
 * @author mandr
 */

public class Employe {
    int id;
    String nom;
    LocalDate dtn;
    Double taille;

    public Double gettaille() {
        return taille;
    }

    public void settaille(Double taille) {
        this.taille = taille;
    }

    public LocalDate getdtn() {
        return dtn;
    }

    public void setdtn(LocalDate dtn) {
        this.dtn = dtn;
    }
    public Employe(){}
    public Employe(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public String getnom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    @AnotURL(url="emp/lister")
    @AnotAction(action="/listeEmp.jsp")
    public ModelView listeremp(){
        ModelView mv=new ModelView();
        ArrayList<Employe> lE=new ArrayList<>();
        Employe e=new Employe(1,"Sonia");
        Employe ee=new Employe(2,"SOSO");
        lE.add(e);
        lE.add(ee);
        HashMap<String,Object> data=new HashMap<String,Object>();
        data.put("donnees",lE);
        mv.setData(data);
        
    return mv;
    }
    
    @AnotURL(url="emp/wala")
    @AnotAction(action="/index.html")
    public void walawala(){
        System.out.print("WALAAA");
    }
    
    @AnotURL(url="emp/saveEmp")
    @AnotAction(action="/newjsp.jsp")
    public ModelView Save(){
        ModelView mv=new ModelView();
        System.out.print("save ok");
        System.out.print("Nom employe: "+this.getnom()+"id Employe: "+this.getid());
       HashMap<String,Object> data=new HashMap<String,Object>();
       data.put("emp",this);
       mv.setData(data);
        return mv;
    }
}
